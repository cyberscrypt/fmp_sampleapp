import React from 'react';
import ReactDOM from 'react-dom';
import Company from './Company';

function Example() {
    return (
        <div className="container">
            <Company/>
        </div>
    );
}

export default Example;

if (document.getElementById('example')) {
    ReactDOM.render(<Example/>, document.getElementById('example'));
}
