import React from "react";
import './company.css';
import {BASE_URL} from '../Common/Common';

class Company extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            companyCode: '',
            error: '',
            companyData: {},
            companyQuotesData: [],
            companyInfoLoading: false,
            companyQuotesLoading: false,
            companyLinksDetails: {},
            getLinkLoading: false,
            dataNotFound:false,
        }
        this.getCompanyDetails = this.getCompanyDetails.bind(this);
        this.getCompanyQuotes = this.getCompanyQuotes.bind(this);
        this.renderTable = this.renderTable.bind(this);
        this.renderTableArrayData = this.renderTableArrayData.bind(this);
        this.renderTableObjectData = this.renderTableObjectData.bind(this);
        this.getComapnyLinks = this.getComapnyLinks.bind(this);
    }

    componentDidMount() {
        this.getComapnyLinks();
    }

    getComapnyLinks() {

        this.setState({getLinkLoading: true})
        const fetchPromise = fetch(`${BASE_URL}get-company-data`);
        fetchPromise.then(response => {
            return response.json();
        }).then(data => {
            console.log(data);
            this.setState({companyLinksDetails: data.data || {}, getLinkLoading: false})
        }).catch(data => {
            this.setState({getLinkLoading: false})
        })
    }


    getCompanyDetails() {
        const {companyCode, companyLinksDetails} = this.state

        // if(!companyCode){
        //     this.setState({error:'Please enter company code'})
        //     return;
        // }

        this.setState({companyInfoLoading: true})
        let url = companyLinksDetails && companyLinksDetails.company_info_url
        const fetchPromise = fetch(url);
        fetchPromise.then(response => {
            return response.json();
        }).then(data => {
            if (data && data["Error Message"]) {
                this.setState({error: data["Error Message"], companyInfoLoading: false})
            } else {
                let compData = data
                data["profile"] = new Array(data["profile"])
                data["metrics"] = new Array(data["metrics"])
                this.setState({companyInfoLoading: false, companyData: data, error: ''})
            }

        }).catch(data => {
            this.setState({companyInfoLoading: false, companyData: data})
        })

    }

    getCompanyQuotes() {

        const {companyCode, companyLinksDetails} = this.state

        // if(!companyCode){
        //     this.setState({error:'Please enter company code'})
        //     return;
        // }

        this.setState({companyQuotesLoading: true})

        let url = companyLinksDetails && companyLinksDetails.company_quotes_url
        const fetchPromise = fetch(url);
        fetchPromise.then(response => {
            return response.json();
        }).then(data => {
            if (data && data["Error Message"]) {
                this.setState({error: data["Error Message"], companyQuotesLoading: false})
            } else {
                this.setState({companyQuotesLoading: false, companyQuotesData: data, error: ''})
            }

        }).catch(data => {
            this.setState({companyQuotesLoading: false, companyData: data})
        })




    }

    renderTable(heading, data) {

        if (Array.isArray(data)) {
            return (
                this.renderTableArrayData(heading, data)
            )
        } else if (typeof data == "object") {
            return (
                this.renderTableObjectData(heading, data)
            )
        }
    }

    renderTableObjectData(heading, data) {
        return (
            <div>
                <div>
                    <div className="title-heading">{heading}</div>
                </div>

                {Object.keys(data).map(key => (
                    <div key={key}>
                        {Array.isArray(data[key]) &&
                        this.renderTableArrayData(key, data[key])
                        }
                    </div>
                ))}
            </div>
        )
    }

    renderTableArrayData(heading, data) {
        let keys = Object.keys(data[0])
        return (
            <table>
                <thead>
                <tr>
                    <td className="title-heading">{heading}</td>
                </tr>
                </thead>

                <tbody>
                <tr>
                    {keys.map((item, index) => {
                        return (
                            <th key={index}>{item}</th>
                        )
                    })}
                </tr>

                {data.map((item, index) => {
                    return (
                        <tr key={index}>
                            {keys.map((k, i) => {
                                return (
                                    <td key={'data' + i}>
                                        {k == 'image' ?
                                            <img src={item[k]} className="table-img"/>
                                            :
                                            item[k]
                                        }
                                    </td>
                                )
                            })}
                        </tr>
                    )
                })}
                </tbody>
            </table>
        )
    }

    render() {
        const {companyCode, error, getLinkLoading,dataNotFound, companyInfoLoading, companyData, companyQuotesLoading, companyQuotesData} = this.state

        console.log(this.state)
        return (
            <div>
                {getLinkLoading ?
                    <div>Loading</div>
                    :
                    <div>
                        <div className="form-main">
                            {/*<input
                                placeholder="Enter company code"
                                value={companyCode}
                                onChange={(e) => this.setState({companyCode: e.target.value, error: ''})}
                                className="form-input"
                            />
                            {error &&
                            <div className="form-error-msg">
                                {error}
                            </div>
                            }*/}

                            <div>
                                {error &&
                                <div className="form-error-msg">
                                    {error}
                                </div>
                                }
                            </div>
                            <div className="form-btn-main">
                                <button
                                    className="btn-primary"
                                    onClick={this.getCompanyDetails}
                                >
                                    {companyInfoLoading ? 'Loading...' : 'Get company info'}
                                </button>
                                <button
                                    className="btn-primary"
                                    onClick={this.getCompanyQuotes}
                                >
                                    { companyQuotesLoading? 'Loading...' : 'Get company quotes'}
                                </button>
                            </div>
                        </div>

                        {companyData.length > 0 &&
                        <div className="tablelist-main" style={{height: 'auto', marginBottom: '16px'}}>
                            {this.renderTableArrayData('Company Profile', companyData)}
                        </div>
                        }
                        
                        {companyQuotesData.length > 0 &&
                        <div className="tablelist-main" style={{height: 'auto', marginBottom: '16px'}}>
                            {this.renderTableArrayData('Company Quotes', companyQuotesData)}
                        </div>
                        }


                    </div>
                }
            </div>
        )
    }
}

export default Company;
