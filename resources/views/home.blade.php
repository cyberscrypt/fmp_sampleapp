@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @if(session()->has('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif

                @if(session()->has('error'))
                    <div class="alert alert-danger">
                        {{ session()->get('error') }}
                    </div>
                @endif

                <div class="card">
                    <div class="card-header">{{ __('Company Urls') }}</div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('url.store') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="company_url"
                                       class="col-md-2 col-form-label text-md-right">{{ __('Company Url') }}</label>

                                <div class="col-md-10">
                                    <input id="company_url" type="url"
                                           class="form-control @error('company_url') is-invalid @enderror"
                                           name="company_url"
                                           value="{{ @$companyInfo->company_info_url ? $companyInfo->company_info_url : '' }}"
                                           required
                                           autocomplete="company_url" autofocus>

                                    @error('company_url')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="company_quote_url"
                                       class="col-md-2 col-form-label text-md-right">{{ __('Company Quote Url') }}</label>

                                <div class="col-md-10">
                                    <input id="company_quote_url" type="url"
                                           class="form-control @error('company_quote_url') is-invalid @enderror"
                                           value="{{ @$companyInfo->company_quotes_url ? $companyInfo->company_quotes_url : '' }}"
                                           name="company_quote_url"
                                           required autocomplete="current-password">

                                    @error('company_quote_url')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-12 offset-md-2">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Update') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="example"></div>
@endsection
