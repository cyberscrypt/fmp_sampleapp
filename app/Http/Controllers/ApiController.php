<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\CompanyInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ApiController extends Controller
{
    public function getCompanyInfo()
    {
        $companyInfo = CompanyInfo::where('user_id', Auth::id())->first();

        if ($companyInfo) {
            return response()->json([
                'status' => 200,
                'message' => 'Success',
                'data' => $companyInfo,
            ]);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'No Data Available',
            ]);
        }
    }
}
