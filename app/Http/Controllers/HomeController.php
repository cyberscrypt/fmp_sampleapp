<?php

namespace App\Http\Controllers;

use App\Models\CompanyInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $companyInfo = CompanyInfo::where('user_id', Auth::id())->first();
        return view('home', compact('companyInfo'));
    }

    public function urlStore(Request $request)
    {
        $request->validate([
            'company_url' => 'required|url',
            'company_quote_url' => 'required|url',
        ]);

        $companyInfo = CompanyInfo::where('user_id', Auth::id())->first();
        if ($companyInfo != null) {
            $companyInfo->company_info_url = $request->company_url;
            $companyInfo->company_quotes_url = $request->company_quote_url;
            $companyInfo->save();
        } else {
            $companyInfo = new CompanyInfo;
            $companyInfo->user_id = Auth::id();
            $companyInfo->company_info_url = $request->company_url;
            $companyInfo->company_quotes_url = $request->company_quote_url;
            $companyInfo->save();
        }
        return redirect()->back()->with('success', 'Data Updated');
    }

    public function getCompanyInfo()
    {
        $companyInfo = CompanyInfo::where('user_id', Auth::id())->first();
        if ($companyInfo) {
            return response()->json([
                'status' => 200,
                'message' => 'Success',
                'data' => $companyInfo,
            ]);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'No Data Available',
            ]);
        }
    }
}
