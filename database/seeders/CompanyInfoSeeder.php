<?php

namespace Database\Seeders;

use App\Models\CompanyInfo;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Auth;

class CompanyInfoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CompanyInfo::create([
            'user_id' => 1,
            'company_info_url' => 'https://financialmodelingprep.com/api/v3/profile/TSLA?apikey=d9cfb18961308e386a60332177a42c8c',
            'company_quotes_url' => 'https://financialmodelingprep.com/api/v3/quote/AAPL?apikey=d9cfb18961308e386a60332177a42c8c',
        ]);
    }
}
